package com.draft.springjwttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJwtTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJwtTestApplication.class, args);
    }

}
