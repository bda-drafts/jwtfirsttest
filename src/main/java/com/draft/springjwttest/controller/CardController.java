package com.draft.springjwttest.controller;

import com.draft.springjwttest.dto.CardDto;
import com.draft.springjwttest.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apiCard")
@CrossOrigin("*")
public class CardController {

    private CardService cardService;

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @PostMapping("/create/person/{personId}/card")
    public ResponseEntity<CardDto> createCard(@PathVariable(value = "personId") Long personId, @RequestBody CardDto cardDto) {
        return new ResponseEntity<>(cardService.createCard(personId, cardDto), HttpStatus.CREATED);
    }

    @GetMapping("/person/{personId}/cards")
    public List<CardDto> getCardsByPersonId(@PathVariable(value = "personId") Long personId) {
        return cardService.getCardsByPersonId(personId);
    }

    @GetMapping("/person/{personId}/card/{id}")
    public ResponseEntity<CardDto> getCardById(@PathVariable(value = "personId") Long personId, @PathVariable(value = "id") Long cardId) {
        CardDto cardDto = cardService.getCardById(cardId, personId);
        return new ResponseEntity<>(cardDto, HttpStatus.OK);
    }

    @PutMapping("/update/person/{personId}/card/{id}")
    public ResponseEntity<CardDto> updateCardById(@PathVariable(value = "personId") Long personId, @PathVariable(value = "id") Long cardId, @RequestBody CardDto cardDto) {
        CardDto newCardDto = cardService.updateCard(personId, cardId, cardDto);
        return new ResponseEntity<>(newCardDto, HttpStatus.OK);
    }

    @DeleteMapping("/delete/person/{personId}/card/{id}")
    public ResponseEntity<String> deleteCard(@PathVariable(value = "personId") Long personId, @PathVariable(value = "id") Long cardId) {
        cardService.deleteCard(personId,cardId);
        return new ResponseEntity<>("Card deleted successfully!", HttpStatus.OK);
    }
}

