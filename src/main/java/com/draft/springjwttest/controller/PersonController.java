package com.draft.springjwttest.controller;


import com.draft.springjwttest.dto.PersonDto;
import com.draft.springjwttest.dto.PersonResponse;
import com.draft.springjwttest.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apiPerson")
@CrossOrigin("*")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("add")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PersonDto> addPerson(@RequestBody PersonDto person) {
        return new ResponseEntity<>(personService.create(person), HttpStatus.CREATED);
    }

    @GetMapping("people")
    public ResponseEntity<PersonResponse> getPersons(
            @RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize
    ) {
        return new ResponseEntity<>(personService.getAllPerson(pageNo, pageSize), HttpStatus.OK);
    }

    @GetMapping("people/{id}")
    public ResponseEntity<PersonDto> personById(@PathVariable Long id){
        return ResponseEntity.ok(personService.getPersonById(id));
    }

    @PutMapping("people/update/{id}")
    public ResponseEntity<PersonDto> updatePerson(@RequestBody PersonDto personDto, @PathVariable("id") Long personId) {
        PersonDto response = personService.updatePerson(personDto, personId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("people/delete/{id}")
    public ResponseEntity<String> deletePerson(@PathVariable Long id) {
        personService.deletePerson(id);
        return new ResponseEntity<>("Person deleted!", HttpStatus.OK);
    }
}
