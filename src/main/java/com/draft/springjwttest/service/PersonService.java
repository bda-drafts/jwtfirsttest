package com.draft.springjwttest.service;

import com.draft.springjwttest.dto.PersonDto;
import com.draft.springjwttest.dto.PersonResponse;

public interface PersonService {
    PersonDto create(PersonDto personDto);
    PersonResponse getAllPerson(int pageNo, int pageSize);
    PersonDto getPersonById(Long id);
    PersonDto updatePerson(PersonDto personDto, Long id);
    void deletePerson(Long id);
}
