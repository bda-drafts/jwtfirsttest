package com.draft.springjwttest.service.impl;

import com.draft.springjwttest.dto.CardDto;
import com.draft.springjwttest.exception.CardNotFoundException;
import com.draft.springjwttest.exception.PersonNotFoundException;
import com.draft.springjwttest.models.Card;
import com.draft.springjwttest.models.Person;
import com.draft.springjwttest.repository.CardRepository;
import com.draft.springjwttest.repository.PersonRepository;
import com.draft.springjwttest.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;

    private final PersonRepository personRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository, PersonRepository personRepository) {
        this.cardRepository = cardRepository;
        this.personRepository = personRepository;
    }

    @Override
    public CardDto createCard(Long personId, CardDto cardDto) {
        Card card = mapToModel(cardDto);

        Person person = personRepository.findById(personId).orElseThrow(() -> new PersonNotFoundException("Person not found"));

        card.setPerson(person);

        Card newCard = cardRepository.save(card);

        return mapToDto(newCard);
    }

    @Override
    public List<CardDto> getCardsByPersonId(Long id) {
        List<Card> cards = cardRepository.findByPerson_Id(id);

        return cards.stream().map(card -> mapToDto(card)).collect(Collectors.toList());
    }

    @Override
    public void deleteCard(Long personId, Long cardId) {
        Card card = cardRepository.findById(cardId).orElseThrow(() -> new CardNotFoundException("Card not found!"));

        Person person = personRepository.findById(personId).orElseThrow(() -> new PersonNotFoundException("Person not found!"));

        if (card.getPerson().getId() != person.getId()) {
            throw new CardNotFoundException("This card not belong to person!");
        }

        cardRepository.delete(card);
    }

    @Override
    public CardDto getCardById(Long cardId, Long personId) {
        Person person = personRepository.findById(personId).orElseThrow(() -> new PersonNotFoundException("Person not found!"));

        Card card = cardRepository.findById(cardId).orElseThrow(() -> new CardNotFoundException("Card not found!"));

        if (card.getPerson().getId() != person.getId()) {
            throw new CardNotFoundException("This card not belong to person!");
        }
        return mapToDto(card);
    }

    @Override
    public CardDto updateCard(Long personId, Long cardId, CardDto cardDto) {
        Person person = personRepository.findById(personId).orElseThrow(() -> new PersonNotFoundException("Person not found!"));

        Card card = cardRepository.findById(cardId).orElseThrow(() -> new CardNotFoundException("Card not found!"));

        if (card.getPerson().getId() != person.getId()) {
            throw new CardNotFoundException("Card does not belong to person!");
        }

        card.setCardName(cardDto.getCardName());
        card.setCardNumber(cardDto.getCardNumber());

        Card newCard = cardRepository.save(card);

        return mapToDto(newCard);
    }

    public CardDto mapToDto (Card card) {
        CardDto cardDto = new CardDto();
        cardDto.setId(card.getId());
        cardDto.setCardName(card.getCardName());
        cardDto.setCardNumber(card.getCardNumber());

        return cardDto;
    }

    public Card mapToModel(CardDto cardDto) {
        Card card = new Card();
        card.setId(cardDto.getId());
        card.setCardName(cardDto.getCardName());
        card.setCardNumber(cardDto.getCardNumber());

        return card;
    }
}
