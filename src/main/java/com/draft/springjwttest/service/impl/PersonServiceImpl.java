package com.draft.springjwttest.service.impl;

import com.draft.springjwttest.dto.PersonDto;
import com.draft.springjwttest.dto.PersonResponse;
import com.draft.springjwttest.exception.PersonNotFoundException;
import com.draft.springjwttest.models.Person;
import com.draft.springjwttest.repository.PersonRepository;
import com.draft.springjwttest.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public PersonDto create(PersonDto personDto) {
        Person person = new Person();
        person.setName(personDto.getName());
        person.setSurname(personDto.getSurname());

        Person newPerson = personRepository.save(person);

        PersonDto personResponse = new PersonDto();

        personResponse.setId(newPerson.getId());
        personResponse.setName(newPerson.getName());
        personResponse.setSurname(newPerson.getSurname());

        return personResponse;
    }

    @Override
    public PersonResponse getAllPerson(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<Person> pageableList = personRepository.findAll(pageable);
        List<Person> listOfPerson = pageableList.getContent();
        //usage map returns a new list with personDto entity
        List<PersonDto> content = listOfPerson.stream().map(person -> mapToDto(person)).collect(Collectors.toList());

        PersonResponse personResponse = new PersonResponse();
        personResponse.setContent(content);
        personResponse.setPageNo(pageableList.getNumber());
        personResponse.setPageSize(pageableList.getSize());
        personResponse.setTotalElements(pageableList.getTotalElements());
        personResponse.setTotalPages(pageableList.getTotalPages());
        personResponse.setLast(personResponse.isLast());

        return personResponse;
    }

    @Override
    public PersonDto getPersonById(Long id) {
        Person person = personRepository.findById(id).orElseThrow(() -> new PersonNotFoundException("Person could not be found with the id of: " + id));
        return mapToDto(person);
    }

    @Override
    public PersonDto updatePerson(PersonDto personDto, Long id) {
        Person person = personRepository.findById(id).orElseThrow(() -> new PersonNotFoundException("Person could not be updated"));

        person.setName(personDto.getName());
        person.setSurname(personDto.getSurname());

        Person updatedPerson = personRepository.save(person);

        return mapToDto(updatedPerson);
    }

    @Override
    public void deletePerson(Long id) {
        Person person = personRepository.findById(id).orElseThrow(() -> new PersonNotFoundException("Person not find or could not be deleted!"));
        personRepository.delete(person);
    }

    private PersonDto mapToDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.setId(person.getId());
        personDto.setName(person.getName());
        personDto.setSurname(person.getSurname());

        return personDto;
    }

    private Person mapToModel(PersonDto personDto) {
        Person person = new Person();
        person.setName(personDto.getName());
        person.setSurname(personDto.getSurname());

        return person;
    }
}
