package com.draft.springjwttest.service;

import com.draft.springjwttest.dto.CardDto;

import java.util.List;

public interface CardService {
    CardDto createCard(Long personId, CardDto cardDto);
    List<CardDto> getCardsByPersonId(Long id);
    void deleteCard(Long personId, Long cardId);
    CardDto getCardById(Long cardId, Long personId);
    CardDto updateCard(Long personId, Long cardId, CardDto cardDto);
}
