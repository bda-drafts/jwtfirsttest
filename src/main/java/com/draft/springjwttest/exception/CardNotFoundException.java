package com.draft.springjwttest.exception;

import java.io.Serial;

public class CardNotFoundException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = 2;

    public CardNotFoundException(String message) {
        super(message);
    }
}
