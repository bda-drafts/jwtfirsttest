package com.draft.springjwttest.exception;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ErrorObject {

    Integer statusCode;
    String message;
    Date timestamp;
}
