package com.draft.springjwttest.repository;

import com.draft.springjwttest.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
