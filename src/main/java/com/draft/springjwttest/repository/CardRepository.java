package com.draft.springjwttest.repository;

import com.draft.springjwttest.models.Card;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CardRepository extends JpaRepository<Card, Long> {
    List<Card> findByPerson_Id(Long person_id);
}
